from sentence_transformer import SentenceTransformer
from news_filter import NewsFilter
import time
import pandas as pd
from sklearn.cluster import DBSCAN
from pprint import pprint
from news_cluster import NewsCluster
from sklearn.neighbors import NearestCentroid, NearestNeighbors, KNeighborsClassifier
import matplotlib.pyplot as plt
from sklearn import metrics
from ggplot import *
import numpy as np


class Solver(object):
    def __init__(self, news_selector=None, transformer=None, model=None):
        if news_selector is None:
            self.news_selector = NewsFilter('data/tweets.pkl')
        else:
            self.news_selector = news_selector
        if transformer is None:
            self.transformer = SentenceTransformer("../w2v_models/fasttext100.bin", avg_words=10)
        else:
            self.transformer = transformer
        self.success_file = 'data/success.txt'
        self.tweet_clusters = []
        self.tweet_queue = []
        self.new_tweets = None
        self.model = model

    def check_new_tweets(self, method=None, tsne=False, model=None):
        while True:
            try:
                with open(self.success_file, 'r') as fin:
                    if fin.readline() == 'ok':
                        print('ok')
                        action = True
                if action:
                    with open(self.success_file, 'w') as fout:
                        fout.write('')
                    self.action(method, tsne, model)
                    action = False
                    # след строчка останавливает
                    # break
                else:
                    time.sleep(5)
            except KeyboardInterrupt:
                break

    def action(self, method=None, tsne=False, model=None):
        news = self.news_selector.filter_news()
        print(len(news))
        self.new_tweets = pd.DataFrame(data=self.transformer.transform_data(news, method, tsne))
        self.new_tweets['Sentence'] = news
        self.clustering(model)

    def clustering(self, model=None):
        print('Start clustering...')
        # print(sent_embeddings[:, 1])
        if model is not None:
            self.model = model
        self.model.fit(self.new_tweets.drop(['Sentence'], axis=1).values)
        labels = self.model.labels_
        n_clusters_ = len(set(labels)) - (1 if -1 in labels else 0)
        print('number of clusters %d' % n_clusters_)
        unique_labels = set(labels)
        print('unique labels %d' % len(unique_labels))
        if len(unique_labels) > 1:
            self.samples_scores = metrics.silhouette_samples(self.new_tweets.drop(['Sentence'], axis=1).values,
                                                             self.model.labels_)
        else:
            self.samples_scores = np.zeros((self.new_tweets.shape[0], 1))
        clusters = 0
        for k in unique_labels:
            class_members = labels == k
            raw_data, vect_data = [], []
            for i, member in enumerate(class_members):
                if member and self.samples_scores[i] > 0.35:
                    raw_data.append(self.new_tweets.Sentence[i])
                    vect_data.append(self.new_tweets.iloc[i].values[:-1])
            if k != -1:
                print('Tweets in cluster %d' % len(raw_data))
                pprint(raw_data)
                print("-" * 40)
                clusters += 1
                if len(raw_data) >= 7:
                    self.tweet_clusters.append(NewsCluster(raw_data, vect_data))

        print('number of clusters %d' % clusters)

    def plot(self):
        vectors = self.new_tweets.drop(['Sentence'], axis=1).values
        if vectors.shape[1] != 2:
            raise Exception
        x = pd.DataFrame(vectors)
        x.columns = ['x', 'y']
        chart = ggplot(x, aes(x='x', y='y')) + geom_point(size=25, alpha=0.8)

        return chart
