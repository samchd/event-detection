import numpy as np
import re

from sklearn.manifold import TSNE
from stop_words import get_stop_words
from datetime import datetime
from fasttext import load_model
from nltk import ngrams


class SentenceTransformer(object):
    def __init__(self, wv_path, method='average', avg_words=12):
        # add if wv_path not exists
        print('Initialising word2vec model...')
        start = datetime.now()
        self.wv_model = load_model(wv_path)
        # self.wv_vocab = set(self.wv_model.vocab)
        print('Initialising is done!')
        print('time:', datetime.now() - start)
        self.stop_words = set()
        for w in get_stop_words('russian'):
            self.stop_words.add(w)
        self.non_letters_pattern = r'[^А-ЯЁа-яё\dA-Za-z-]'
        self.non_cyrillic_pattern = r'[^А-ЯЁа-яё]'
        self.num_features = self.wv_model.dim
        self.avg_words = avg_words
        self.method = method

    def concat_feature_vector(self, sentence):
        feature_vec = []
        words_count = 0
        sentence = sentence.lower()
        for word in re.sub(self.non_letters_pattern, ' ', sentence).split():
            if word not in self.stop_words:
                feature_vec.append(self.wv_model[word])
                # feature_vec.append(np.ones(5))
                words_count += 1
                if words_count == self.avg_words:
                    return np.concatenate(feature_vec)
        if words_count >= self.avg_words:
            print('Something went wrong!!!')
            return np.concatenate(feature_vec)
        else:
            feature_vec.append(np.zeros((self.avg_words - words_count) * self.num_features))
            return np.concatenate(feature_vec)

    def avg_feature_vector(self, sentence):
        feature_vec = np.zeros((self.num_features,), dtype='float32')
        n_words = 0
        sentence = sentence.lower()
        for word in re.sub(self.non_cyrillic_pattern, ' ', sentence).split():
            if word not in self.stop_words:
                n_words += 1
                feature_vec = np.add(feature_vec, self.wv_model[word])
        if n_words > 0:
            feature_vec = np.divide(feature_vec, n_words)
        return feature_vec

    def fast_feature_vector(self, sentence):
        feature_vec = np.zeros((self.num_features,), dtype='float32')
        sent = ''
        sentence = sentence.lower()
        for word in re.sub(self.non_cyrillic_pattern, ' ', sentence).split():
            # if word not in self.stop_words:
                sent += word + ' '
        if len(sent) > 0:
            feature_vec = self.wv_model[sent]
        return feature_vec

    def char_feature_vector(self, sentence):
        feature_vec = np.zeros((self.num_features,), dtype='float32')
        sent = ''
        sentence = sentence.lower()
        n_words = 0
        for word in re.sub(self.non_cyrillic_pattern, ' ', sentence).split():
            if word not in self.stop_words:
                sent += word + ' '
        for x in ngrams(sent, 5):
            word = ''.join(x)
            n_words += 1
            feature_vec = np.add(feature_vec, self.wv_model[word])
        if n_words > 0:
            feature_vec = np.divide(feature_vec, n_words)
        return feature_vec

    def transform_data(self, data, method=None, tsne=False):
        # Сделать что-то с первой строчкой
        vectors = []
        if method is not None:
            self.method = method

        if self.method == 'average':
            for i, sent in enumerate(data):
                vectors.append(self.avg_feature_vector(sent))

        if self.method == 'all':
            for i, sent in enumerate(data):
                vectors.append(self.fast_feature_vector(sent))

        if self.method == 'chars':
            for i, sent in enumerate(data):
                # vectors.append(self.concat_feature_vector(sent))
                vectors.append(self.char_feature_vector(sent))

        if tsne:
            tsne = TSNE(n_components=2, verbose=1, perplexity=40, n_iter=300)
            return tsne.fit_transform(vectors)

        return np.array(vectors)
