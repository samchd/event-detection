from sklearn.pipeline import Pipeline
import pickle
import re
import numpy as np


class NewsFilter(object):
    def __init__(self, filepath):
        self.reference_pattern = "@[\S]*"
        self.link_pattern = r'https?:\/\/\S.*'
        self.pic_pattern = r'pic.twitter.com/\S*'
        self.other_pattern = r'(?:RT| via|\(via )'
        self.patterns = [self.reference_pattern, self.link_pattern, self.pic_pattern, self.other_pattern]
        self.filepath = filepath
        self.success_file = 'data/success.txt'
        # with open("model/tf_2_fit.model", "rb") as fin:
        with open("model/tf_2_fit.model", "rb") as fin:
            cv = pickle.load(fin)
        with open("model/linear_svc.model", "rb") as fin:
            svc = pickle.load(fin)
        self.pipeline = Pipeline([('CountVectorizer', cv), ('LogisticRegression', svc)])

    def read_tweets(self):
        with open(self.filepath, 'rb') as fin:
            raw_tweets = pickle.load(fin)
        print('Read %d tweets' % len(raw_tweets))
        return np.asarray(raw_tweets)

    def preprocessing(self):
        cleaned_data, raw_data = [], []
        for line in self.read_tweets():
            cleaned_line = line
            for pattern in self.patterns:
                cleaned_line = re.sub(pattern, '', cleaned_line)
            cleaned_data.append(cleaned_line)
            raw_data.append(line)

        return np.asarray(cleaned_data), np.asarray(raw_data)

    def filter_news(self):
        cleaned_data, raw_data = self.preprocessing()
        probas = self.pipeline.predict(cleaned_data)

        return raw_data[np.where(probas == 1)]
