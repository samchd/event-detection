from datetime import time


class NewsCluster(object):
    def __init__(self, raw_tweets, vect_tweets):
        self.raw_tweets = raw_tweets
        self.vect_tweets = vect_tweets
        self.born_time = time()
