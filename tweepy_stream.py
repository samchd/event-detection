import json
import tempfile
from textwrap import TextWrapper

import errno
import tweepy
from datetime import datetime, timedelta
from pprint import pprint
import subprocess
import os
import pickle
import logging


class StreamWatcherListener(tweepy.StreamListener):

    def __init__(self):
        super().__init__()
        # self.file_obj = open("data/tweets.txt", "a")
        self.time_end = datetime.now() + timedelta(minutes=5)
        self.tweets = []
        self.count_tweets = 0
        self.file_path = "data/tweets.pkl"
        if not os.path.exists('data'):
            os.makedirs('data')
        self.filename = 'asd.fifo'
        self.start_time = datetime.now()
        self.success_file = 'data/success.txt'
        # if os.path.exists(self.fifo_path):
        #     os.remove(self.fifo_path)

        # os.mkfifo(self.fifo_path)

        # tmpdir = tempfile.mkdtemp()
        # self.fifo_path = os.path.join(tmpdir, 'my_fifo')
        # print(self.fifo_path)
        # os.mkfifo(self.fifo_path)

    # def on_data(self, raw_data):
    #     try:
    #         # print(self.status_wrapper.fill(status.text))
    #         # print('\n %s  %s  via %s\n' % (status.author.screen_name, status.created_at, status.source))
    #         data = json.loads(raw_data)
    #         pprint(data)
    #     except Exception as e:
    #         print(e)

    def send_data(self):
        logging.info('tweets count: %d' % self.count_tweets)
        self.count_tweets = 0
        start = datetime.now()
        with open(self.file_path, 'wb') as fout:
            pickle.dump(self.tweets, fout)
        self.tweets = []
        with open(self.success_file, 'w') as fout:
            fout.write('ok')
        logging.info("Sending data... success\n")

    def on_status(self, status):
        # self.file_obj.write(status.text + "\n")
        # json_text = json.dumps({"text": status.text, "id": status.id})
        # print(status.text)
        self.tweets.append(status.text)
        self.count_tweets += 1

        if self.count_tweets >= 3000:
            logging.info('time for collect tweets: %s' % str(datetime.now() - self.start_time))
            self.start_time = datetime.now()
            self.send_data()
        # print(json_text)
        # self.file_obj.write(json_text + '\n')
        # if datetime.now() <= self.time_end:
        #     return True
        # else:
        #     return False

        # outputOfOtherProcess = subprocess.check_output([
        #     './test.py'],
        #     shell=True)
        # subprocess.call('python test.py', shell=True)
        # print(outputOfOtherProcess)

        # p = subprocess.Popen('python test.py', shell=True)
        # p.wait()

        return True

    def on_error(self, status_code):
        logging.error('An error has occured! Status code = %s' % status_code)
        return True  # keep stream alive

    def on_timeout(self):
        print('Snoozing Zzzzzz')

    # def __del__(self):
        # self.file_obj.close()


def main():
    # Prompt for login credentials and setup stream object
    with open('config.json') as config_data:
        config = json.load(config_data)

    auth = tweepy.auth.OAuthHandler(config['consumer_key'], config['consumer_secret'])
    auth.set_access_token(config['access_token'], config['access_token_secret'])
    stream = tweepy.Stream(auth, StreamWatcherListener())

    # stream.sample(languages=['ru'])

    # stream.filter(locations=[81, 104, 41, 47, 54, 19, 65, -169], languages=['ru'], async=True)
    # stream.filter(locations=[56.95,37.7, 54.25,38.63], async=True)
    vocab = []
    with open('vocab/vocab.txt') as fin:
        for line in fin:
            vocab.append(line.strip())
    print(vocab[:5])
    # print(len(vocab))

    stream.filter(track=vocab[:400])


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)
    try:
        main()
    except KeyboardInterrupt:
        print('\nGoodbye!')
